/* 
 * Napiste program, ve kterem vytvorite novy datovy typ "Car", 
 * ktery bude mit vlastnosti "maximalniRychlost", "nazev"
 * a alespon 3 dalsi vlastnosti (napr: rokVyroby, rozmerKola,...). 
 * Zamyslete se, jake datove typy pouzijete.
 * 
 * Pote vytvorte pole nebo seznam, kde bude alespon 5 aut.
 * Vypiste nej-auto podle alespon dvou ruznych vlastnosti.
 *      Napr: 
 *          nejstarsi auto je VolksWagen Golf, rok vyroby 1993
 *          nejmensi kola ma City smart, rozmer jen 14.3
 *          
 * Nakonec pole seradte od nejrychlejsich aut po nejpomalejsi 
 * a serazene pole vypiste na obrazovku
 */


using System;

class Program
{
    public struct Car
    {
        // much empty
    }

    public static void Main(string[] args)
    {
        // such blank
    }
}
